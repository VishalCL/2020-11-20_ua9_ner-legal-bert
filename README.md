# NER on legal texts with Bert

This repository contains the notebooks and handout for the following presentation:

title
: NER on legal texts with Bert

author
: Thomas Timmermann

date
: 2020-11-20

occasion
: 9th Data Science UA Conference

## Usage

- clone the repository and run locally or
- run the notebooks on binder: https://mybinder.org/v2/gl/thomtimm%2F2020-11-20_ua9_ner-legal-bert/master
